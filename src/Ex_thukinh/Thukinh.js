import React, { Component } from "react";
import { data } from "./Data";

export default class Thukinh extends Component {
  constructor(){
    super();
    this.state = {
      matkinh: null
    }
  }
  bgImg = "./thukinh/Product/ImageProduct/background.jpg";
  render() {
    return (
      <div>
        <div style={{backgroundImage:`url(${this.bgImg})`}}>
          <div style={{
            padding: "20px 0 100px 250px"
          }}>
            <div style={{ display: "flex" }}>
              <img src="./thukinh/Product/ImageProduct/model.jpg" alt="" />
              <div style={{position:'relative'}}>
                <img src="./thukinh/Product/ImageProduct/model.jpg" alt="" />
                <img src={this.state.matkinh} alt=""style={{
                  position: "absolute",
                  top: "97px",
                  left: "41px",
                
                }}/>
              </div>
            </div>
            <div>
              {data.map((item) => {
                return <img src={item.url} alt="" key={item.id} onClick = {()=>{this.setState({matkinh:item.url})}}/>;
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
